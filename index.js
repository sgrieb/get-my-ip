'use strict';

require('dotenv').config()
var nodemailer = require('nodemailer');
var getIP = require('external-ip')();
var fs = require('fs');    
var cronJob = require('cron').CronJob;
 
var configFile = 'current.json';

// make sure env is set
if (process.env.email && process.env.pw && process.env.to) {


    var job = new cronJob({
        cronTime: '00 00 10 * * * *',
        onTick: function() {
            console.log('Time: ' + new Date().toLocaleTimeString());
            return;
            // Runs everyday
            // at exactly 12:00:00 AM.
            getIP(function (err, ip) {
                if (err) {
                    console.info(err);
                }

                fs.readFile(configFile, function(err, current){
                    if(err){
                        console.info(err);
                    }

                    // might have errored because it doesnt exist
                    // write either way
                    if(current) {
                        current = JSON.parse(current);
                    }

                    if (!current || current.ip !== ip) {
                        console.log('Writing file with IP: ' + ip);
                        fs.writeFileSync(configFile, JSON.stringify({ip:ip}));
                    }

                    var transporter = nodemailer.createTransport({
                        service: 'Gmail',
                        auth: {
                            user: process.env.email,
                            pass: process.env.pw
                        }
                    });

                    var mailOptions = {
                        from: process.env.email,
                        to: process.env.to,
                        subject: 'IP Changed', // Subject line
                        text: 'Oh hey there! Your IP has changed to: ' + ip
                    };

                    transporter.sendMail(mailOptions, function(error, info){
                        if(error){
                            console.log(error);
                        }
                        else{
                            console.log('Message sent: ' + info.response);
                        };
                    });
                    
                });
            });
        },
        start: false,
        timeZone: "America/New_York"
    });

    job.start();
    console.log('Job started!');
}
else {
    throw Error('Set email, pw, and to in env!');
}
